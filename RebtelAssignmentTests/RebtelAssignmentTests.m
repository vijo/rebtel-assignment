//
//  RebtelAssignmentTests.m
//  RebtelAssignmentTests
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RANetworking.h"
#import "RAStoreCoordinator.h"
#import "RACountry.h"

@interface RebtelAssignmentTests : XCTestCase

@end

@implementation RebtelAssignmentTests
{
    dispatch_semaphore_t semaphore;
}

- (void)setUp {
    [super setUp];
    // semaphore used when testing callback functionality
//    semaphore = dispatch_semaphore_create(0);
}

- (void)tearDown {
//    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    [super tearDown];
}

- (void)testBorders {
    RAStoreCoordinator *storeCoordinator = [RAStoreCoordinator sharedInstance];
    NSManagedObjectContext *moc = storeCoordinator.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Country"];
    NSError *error = nil;
    request.predicate = [NSPredicate predicateWithFormat:@"alpha2Code == %@", @"SE"];
    NSArray *result = [moc executeFetchRequest:request error:&error];
    RACountry *country = [result firstObject];
    
    NSArray *borders = (NSArray *)country.borders;
    
    NSPredicate *borderPredicate = [NSPredicate predicateWithFormat:@"alpha3Code IN %@", borders];
    request.predicate = borderPredicate;
    NSArray *borderArray = [moc executeFetchRequest:request error:&error];
    for (RACountry *country in borderArray) {
        NSLog(@"border: %@", country.nativeName);
    }
    XCTAssertTrue(borderArray.count > 0);
}

- (void)testRegions {
    RAStoreCoordinator *storeCoordinator = [RAStoreCoordinator sharedInstance];
    NSManagedObjectContext *moc = storeCoordinator.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Region"];
    
    NSError *error = nil;
    NSArray *result = [moc executeFetchRequest:request error:&error];
    NSLog(@"regions %ld", result.count);
    for (RARegion *region in result) {
        NSLog(@"region: %@", region.countries);
    }
    XCTAssertNotNil(result);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
