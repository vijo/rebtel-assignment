//
//  RAStoreCoordinator.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 24/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "RARegion.h"

@interface RAStoreCoordinator : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedInstance;
- (void)saveContext;

- (void)cacheCountries:(NSArray *)countries;

@end
