//
//  RACountry+CoreDataProperties.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 28/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RACountry+CoreDataProperties.h"

@implementation RACountry (CoreDataProperties)

@dynamic borders;
@dynamic callingCode;
@dynamic capital;
@dynamic demonym;
@dynamic name;
@dynamic nativeName;
@dynamic population;
@dynamic flagSmall;
@dynamic alpha2Code;
@dynamic alpha3Code;
@dynamic region;

@end
