//
//  RADetailViewModel.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 28/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RAListViewModel.h"

@class RACountry;

@interface RADetailViewModel : NSObject

@property (nonatomic, copy, readonly) NSString *population;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *nativeName;
@property (nonatomic, copy, readonly) NSString *capital;
@property (nonatomic, copy, readonly) NSString *borders;
@property (nonatomic, strong) UIImage *flag;
@property (nonatomic, weak) id<RAUpdaterDelegate>delegate;

- (instancetype)initWithCountry:(RACountry *)country;

@end
