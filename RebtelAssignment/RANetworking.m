//
//  RANetworking.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//
#define COUNTRY_API_URL @"https://restcountries.eu/rest/v1/"
#define FLAG_API_URL    @"https://raw.githubusercontent.com/hjnilsson/country-flags/master/png250px/"

#import "RANetworking.h"
#import "RAStoreCoordinator.h"
#import "RACountry.h"
#import <UIKit/UIKit.h>

@interface RANetworking ()
@property (nonatomic, strong, readonly) NSURLSession *session;
@property (nonatomic, strong, readonly) NSURL *countryURL;
@property (nonatomic, strong, readonly) NSURL *flagURL;
@end

@implementation RANetworking

+ (instancetype)sharedNetworking {
    
    static RANetworking *networking = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        networking = [[self alloc] init];
    });
    return networking;
}

- (NSURLSession *)session {
    
    static NSURLSession *urlSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    });
    return urlSession;
}

- (NSURL *)countryURL {
    return [NSURL URLWithString:COUNTRY_API_URL];
}

- (NSURL *)flagURL {
    return [NSURL URLWithString:FLAG_API_URL];
}

- (void)countries:(completionHandler)completion {
    
    [self country:@"all" completion:completion];
}

- (void)country:(NSString *)callingCode completion:(completionHandler)completion {

    NSString *path;
    if ([callingCode isEqualToString:@"all"]) {
        path = @"all";
    } else {
        path = [NSString stringWithFormat:@"callingcode/%@", callingCode];
    }
    
    NSURL *url = [self.countryURL URLByAppendingPathComponent:path];
    [self contryArrayFromURL:url completion:completion];
}

- (void)contryArrayFromURL:(NSURL *)url completion:(completionHandler)completion {

    NSURLSessionDataTask *task = [self.session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
        if ((error && error.code != -999) || [(NSHTTPURLResponse *)response statusCode] != 200) {
            // error occured, but not because task was cancelled. try to use cached version
            completion(NO, error);
        } else if (error && error.code == -999) {
            // error occured because of task cancellation. we don't want to complete that.
            completion(YES, nil);
        } else {
            // everything ok
            NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                [RAStoreCoordinator.sharedInstance cacheCountries:array];
            });
            completion(NO, nil);
        }
    }];

    [self.session getAllTasksWithCompletionHandler:^(NSArray<__kindof NSURLSessionTask *> * _Nonnull tasks) {
        for (NSURLSessionTask *runningTask in tasks) {
            if ([runningTask.currentRequest.URL isEqual:task.currentRequest.URL]) {
                // task is already running - cancel it
                [task cancel];
                break;
            }
        }
    }];
    UIApplication.sharedApplication.networkActivityIndicatorVisible = YES;
    [task resume];
}

- (void)flagForCountry:(RACountry *)country completion:(void (^)(BOOL))completion {
    NSString *pathComponent = [[country.alpha2Code stringByAppendingString:@".png"] lowercaseString];
    NSURL *url = [self.flagURL URLByAppendingPathComponent:pathComponent];
    
    [[self.session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if ([(NSHTTPURLResponse *)response statusCode] == 200) {
            // got data for current flag, store it!
            country.flagSmall = data;
            completion(YES);
        } else {
            // flag not found, return nothing
            completion(NO);
        }
    }] resume];
}

@end