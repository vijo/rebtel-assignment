//
//  RAListViewCell.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RAListViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIImageView *flagView;
@property (nonatomic, strong) IBOutlet UILabel *countryLabel;
@end
