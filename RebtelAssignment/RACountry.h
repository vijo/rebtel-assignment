//
//  RACountry.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 27/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RARegion;

NS_ASSUME_NONNULL_BEGIN

@interface RACountry : NSManagedObject

+ (instancetype)countryWithDictionary:(NSDictionary *)dictionary;
- (void)updateWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END

#import "RACountry+CoreDataProperties.h"
