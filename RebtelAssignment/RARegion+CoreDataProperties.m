//
//  RARegion+CoreDataProperties.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 28/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RARegion+CoreDataProperties.h"

@implementation RARegion (CoreDataProperties)

@dynamic name;
@dynamic countries;

@end
