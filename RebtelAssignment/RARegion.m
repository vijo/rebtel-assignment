//
//  RARegion.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 27/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RARegion.h"
#import "RACountry.h"
#import "RAStoreCoordinator.h"

@implementation RARegion

+ (instancetype)regionWithName:(NSString *)name {
    return [[self alloc] initWithName:name];
}

- (instancetype)initWithName:(NSString *)name {
    NSManagedObjectContext *moc = [RAStoreCoordinator.sharedInstance managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Region" inManagedObjectContext:moc];
    if (self = [super initWithEntity:entity insertIntoManagedObjectContext:moc]) {
        self.name = name;
    }
    return self;
}

@end
