//
//  RARegion+CoreDataProperties.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 28/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RARegion.h"

NS_ASSUME_NONNULL_BEGIN

@interface RARegion (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSSet<RACountry *> *countries;

@end

@interface RARegion (CoreDataGeneratedAccessors)

- (void)addCountriesObject:(RACountry *)value;
- (void)removeCountriesObject:(RACountry *)value;
- (void)addCountries:(NSSet<RACountry *> *)values;
- (void)removeCountries:(NSSet<RACountry *> *)values;

@end

NS_ASSUME_NONNULL_END
