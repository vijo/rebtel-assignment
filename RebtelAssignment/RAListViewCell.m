//
//  RAListViewCell.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RAListViewCell.h"

@implementation RAListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.flagView.layer.cornerRadius = self.flagView.frame.size.width * .5;
    self.flagView.layer.masksToBounds = YES;
}

- (void)prepareForReuse {
    NSString *path = [NSBundle.mainBundle pathForResource:@"unknownFlag@2x" ofType:@"png"];
    NSData *imageNotFound = [[NSData alloc] initWithContentsOfFile:path];
    self.flagView.image = [UIImage imageWithData:imageNotFound scale:0];
}

@end
