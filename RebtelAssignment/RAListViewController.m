//
//  RAListViewController.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RAListViewController.h"
#import "RAListViewCell.h"
#import "RADetailViewController.h"
#import "RACountry.h"
#import "RAStoreCoordinator.h"

@interface RAListViewController ()
@property (nonatomic, strong) RAListViewModel *viewModel;
@end

@implementation RAListViewController

- (RAListViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[RAListViewModel alloc] initWithManagedObjectContext:[RAStoreCoordinator.sharedInstance managedObjectContext]];
        _viewModel.delegate = self;
    }
    return _viewModel;
}

- (void)viewDidLoad {
    // since there is STILL a bug since iOS7 when swiping back we need to clear selections manually
    self.clearsSelectionOnViewWillAppear = NO;
    [_segmentedControl addTarget:self action:@selector(willToggleSort:) forControlEvents:UIControlEventValueChanged];
    self.title = @"Countries";
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // clear the selected row to fix iOS bug
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.viewModel.numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfItemsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RAListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(RAListViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    RACountry *country = [self.viewModel itemForIndexPath:indexPath];
    cell.countryLabel.text = country.name;
    if (country.flagSmall) {
        cell.flagView.image = [UIImage imageWithData:country.flagSmall scale:0];        
    }
}

#pragma mark - Table view delegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (_segmentedControl.selectedSegmentIndex == 0) return nil;
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    header.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 0, 0)];
    headerLabel.font = [UIFont boldSystemFontOfSize:20];
    headerLabel.text = [self.viewModel nameForSection:section];
    [headerLabel sizeToFit];
    [header addSubview:headerLabel];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_segmentedControl.selectedSegmentIndex == 0) return CGFLOAT_MIN;
    
    return 30.;
}

#pragma mark - Navigation

- (void)willToggleSort:(id)sender {
    [self.viewModel toggleSort:(RASortToggle)[(UISegmentedControl *)sender selectedSegmentIndex]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    RADetailViewController *detailVC = [segue destinationViewController];
    RADetailViewModel *detailViewModel = [[RADetailViewModel alloc] initWithCountry:[self.viewModel itemForIndexPath:indexPath]];
    detailViewModel.delegate = detailVC;
    detailVC.viewModel = detailViewModel;
}

#pragma mark - RAUpdater delegate

- (void)updateUI {
    // method called when api call is done.
    UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
    [self.tableView reloadData];
}

- (void)updateIndexPath:(NSIndexPath *)indexPath {
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

@end
