//
//  RAListViewModel.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum {
    RASortRegional,
    RASortAlphabetical
} RASortToggle;

@class RACountry;

@protocol RAUpdaterDelegate <NSObject>
- (void)updateUI;
@optional
- (void)updateIndexPath:(NSIndexPath *)indexPath;
@end

@interface RAListViewModel : NSObject <NSFetchedResultsControllerDelegate>
@property (nonatomic, weak) id<RAUpdaterDelegate>delegate;

- (instancetype)initWithManagedObjectContext:(NSManagedObjectContext *)context;

- (NSInteger)numberOfSections;
- (NSInteger)numberOfItemsInSection:(NSInteger)section;
- (RACountry *)itemForIndexPath:(NSIndexPath *)indexPath;
- (NSString *)nameForSection:(NSInteger)section;

- (void)toggleSort:(RASortToggle)sort;

@end
