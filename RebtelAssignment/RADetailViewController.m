//
//  RADetailViewController.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 25/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RADetailViewController.h"
#import "RACountry.h"
#import "RANetworking.h"

@interface RADetailViewController ()

@end

@implementation RADetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Info";
    [self configureView];
    self.view.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1.0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)configureView {
    self.capitalLabel.text =    _viewModel.capital;
    self.nameLabel.text =       _viewModel.name;
    self.nativeNameLabel.text = _viewModel.nativeName;
    self.populationLabel.text = _viewModel.population;
    self.bordersLabel.text =    _viewModel.borders;
    self.flagImageView.image =  _viewModel.flag;
}

- (void)updateUI {
    [self configureView];
}

@end
