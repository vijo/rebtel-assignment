//
//  RAStoreCoordinator.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 24/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//
// Singleton class for thread safety while managing Core Data

#import "RAStoreCoordinator.h"
#import "RACountry.h"

@implementation RAStoreCoordinator

+ (instancetype)sharedInstance {
    
    static RAStoreCoordinator *sharedCoordinator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCoordinator = [[self alloc] init];
    });
    return sharedCoordinator;
}

- (void)cacheCountries:(NSArray *)countries {
    NSMutableDictionary *regions = [NSMutableDictionary dictionary];
    
    for (int i=0; i<countries.count; i++) {
        [self cacheCountry:countries[i] regions:regions];
    }
}

- (void)cacheCountry:(NSDictionary *)dictionary regions:(NSMutableDictionary *)regions {
    
    NSManagedObjectContext *moc = self.managedObjectContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Country"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"alpha2Code == %@", [dictionary valueForKeyPath:@"alpha2Code"]];
    request.predicate = predicate;
    
    NSError *error = nil;
    NSArray *result = [moc executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"error fetching country! - %@", error.localizedDescription);
    }
    
    RACountry *country = nil;
    
    if (!error && result.count) {
        country = [result firstObject];
        // update the country document
    } else {
        // create new country cache document
        country = [RACountry countryWithDictionary:dictionary];
    }
    
    if ([regions objectForKey:[dictionary valueForKey:@"region"]]) {
        country.region = [regions objectForKey:[dictionary valueForKey:@"region"]];
    } else {
        RARegion *region = [self regionWithName:[dictionary valueForKey:@"region"]];
        country.region = region;
        [regions setObject:region forKey:[dictionary valueForKey:@"region"]];
    }
}

- (RARegion *)regionWithName:(NSString *)name {

    NSString *regionName = @"Unsorted Region";
    if (name.length) regionName = name;
    
    NSManagedObjectContext *moc = self.managedObjectContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Region"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", regionName];
    request.predicate = predicate;
    
    NSError *error = nil;
    NSArray *result = [moc executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Error fetching region - %@", error.localizedDescription);
        return nil;
    }
    if (result.count) {
        return [result firstObject];
    }
    return [RARegion regionWithName:regionName];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "se.flashist._zapme" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"DataModel.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

@end
