//
//  RAListViewController.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RAListViewModel.h"

@interface RAListViewController : UITableViewController <RAUpdaterDelegate>

@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentedControl;

@end
