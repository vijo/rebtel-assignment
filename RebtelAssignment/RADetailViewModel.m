//
//  RADetailViewModel.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 28/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RADetailViewModel.h"
#import "RACountry.h"
#import "RAStoreCoordinator.h"
#import "RANetworking.h"

@interface RADetailViewModel ()
@property (nonatomic, strong) RACountry *country;
@end

@implementation RADetailViewModel

- (instancetype)initWithCountry:(RACountry *)country {
    if (self = [super init]) {
        _country = country;
        [self updateCountryData:_country];
    }
    return self;
}

- (void)updateCountryData:(RACountry *)country {
    // fetch and update the country info
    UIApplication.sharedApplication.networkActivityIndicatorVisible = YES;
    RANetworking *networking = [[RANetworking alloc] init];
    [networking country:country.callingCode.stringValue completion:^(BOOL cancelled, NSError *error) {
        UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate updateUI];
        });
    }];
}

- (NSString *)name {
    return _country.name;
}

- (NSString *)nativeName {
    return _country.nativeName;
}

- (NSString *)capital {
    return _country.capital;
}

- (NSString *)population {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.usesGroupingSeparator = YES;
    formatter.groupingSeparator = @" ";
    formatter.groupingSize = 3;
    return [formatter stringFromNumber:_country.population];
}

- (NSString *)borders {
    NSArray *borders = (NSArray *)_country.borders;

    // Country has no borders. Return "None"
    if (!borders.count) return @"None";
    
    NSManagedObjectContext *moc = [RAStoreCoordinator.sharedInstance managedObjectContext];

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Country"];
    NSPredicate *borderPredicate = [NSPredicate predicateWithFormat:@"alpha3Code IN %@", borders];
    request.predicate = borderPredicate;
    NSError *error = nil;
    NSArray *borderArray = [moc executeFetchRequest:request error:&error];
    NSString *borderString = @"";
    if (error) {
        NSLog(@"Unable to retrieve border - %@", error.localizedDescription);
        return @"Borders: Failed to retrieve borders";
    } else {
        for (RACountry *country in borderArray) {
            if (borderString.length) {
                borderString = [borderString stringByAppendingString:@", "];
            }
            borderString = [borderString stringByAppendingString:country.name];
        }
    }
    return borderString;
}

- (UIImage *)flag {
    if (_country.flagSmall) {
        return [UIImage imageWithData:_country.flagSmall scale:0];
    }
    NSString *path = [NSBundle.mainBundle pathForResource:@"unknownFlag@2x" ofType:@"png"];
    NSData *imageNotFound = [[NSData alloc] initWithContentsOfFile:path];
    return [UIImage imageWithData:imageNotFound scale:0];
}

@end
