//
//  RADetailViewController.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 25/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RADetailViewModel.h"
@class RACountry;

@interface RADetailViewController : UIViewController <RAUpdaterDelegate>

@property (nonatomic, strong) RADetailViewModel *viewModel;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *nativeNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *populationLabel;
@property (nonatomic, strong) IBOutlet UILabel *capitalLabel;
@property (nonatomic, strong) IBOutlet UILabel *bordersLabel;
@property (nonatomic, strong) IBOutlet UIImageView *flagImageView;

@end
