//
//  RANetworking.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RACountry;
typedef void(^completionHandler)(BOOL cancelled, NSError *error);
@interface RANetworking : NSObject

+ (instancetype)sharedNetworking;

- (void)countries:(completionHandler)completion;
- (void)country:(NSString *)callingCode completion:(completionHandler)completion;

- (void)flagForCountry:(RACountry *)country completion:(void(^)(BOOL success))completion;
@end