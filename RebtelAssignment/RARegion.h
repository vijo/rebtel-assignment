//
//  RARegion.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 27/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RACountry;

NS_ASSUME_NONNULL_BEGIN

@interface RARegion : NSManagedObject

+ (instancetype)regionWithName:(NSString *)name;

@end

NS_ASSUME_NONNULL_END

#import "RARegion+CoreDataProperties.h"
