//
//  RACountry.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 27/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RACountry.h"
#import "RARegion.h"
#import "RAStoreCoordinator.h"

@implementation RACountry

+ (instancetype)countryWithDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithDictionary:dictionary];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    
    NSManagedObjectContext *moc = [RAStoreCoordinator.sharedInstance managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Country" inManagedObjectContext:moc];
    if (self = [super initWithEntity:entity insertIntoManagedObjectContext:moc]) {
        [self updateWithDictionary:dictionary];
    }
    return self;
}

- (void)updateWithDictionary:(NSDictionary *)dictionary {
    self.borders =      [dictionary valueForKey:@"borders"];
    self.capital =      [dictionary valueForKey:@"capital"];
    self.demonym =      [dictionary valueForKey:@"demonym"];
    self.alpha2Code =   [dictionary valueForKey:@"alpha2Code"];
    self.alpha3Code =   [dictionary valueForKey:@"alpha3Code"];
    self.name =         [dictionary valueForKey:@"name"];
    self.nativeName =   [dictionary valueForKey:@"nativeName"];
    self.population =   [dictionary valueForKey:@"population"];
    
    NSNumber *callingCode = [NSNumber numberWithInteger:[[[dictionary valueForKey:@"callingCodes"] firstObject] integerValue]];
    self.callingCode = callingCode;
}

@end
