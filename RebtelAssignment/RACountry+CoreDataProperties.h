//
//  RACountry+CoreDataProperties.h
//  RebtelAssignment
//
//  Created by Joan Vieru on 28/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "RACountry.h"

NS_ASSUME_NONNULL_BEGIN

@interface RACountry (CoreDataProperties)

@property (nullable, nonatomic, retain) id borders;
@property (nullable, nonatomic, retain) NSNumber *callingCode;
@property (nullable, nonatomic, retain) NSString *capital;
@property (nullable, nonatomic, retain) NSString *demonym;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *nativeName;
@property (nullable, nonatomic, retain) NSNumber *population;
@property (nullable, nonatomic, retain) NSData *flagSmall;
@property (nullable, nonatomic, retain) NSString *alpha2Code;
@property (nullable, nonatomic, retain) NSString *alpha3Code;
@property (nullable, nonatomic, retain) RARegion *region;

@end

NS_ASSUME_NONNULL_END
