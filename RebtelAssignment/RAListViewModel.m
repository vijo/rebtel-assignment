//
//  RAListViewModel.m
//  RebtelAssignment
//
//  Created by Joan Vieru on 22/08/16.
//  Copyright © 2016 Joan Vieru. All rights reserved.
//

#import "RAListViewModel.h"
#import "RANetworking.h"
#import "RACountry.h"
#import "RARegion.h"

@interface RAListViewModel ()
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;
@end

@implementation RAListViewModel
{
    RASortToggle _sort;
}

- (instancetype)initWithManagedObjectContext:(id)context {
    if (self = [super init]) {
        _managedObjectContext = context;
        NSError *error = nil;
        [self.fetchedResultsController performFetch:&error];
        [self updateCache];
        if (error) {
            NSLog(@"Error performing fetch! %@", error.localizedDescription);
        }
    }
    return self;
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) return _fetchedResultsController;
    
    [NSFetchedResultsController deleteCacheWithName:@"Root"];

    NSManagedObjectContext *moc = self.managedObjectContext;

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Country"];
    request.fetchBatchSize = 20;
    
    NSSortDescriptor *countrySort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];

    NSFetchedResultsController *frc = nil;
    
    if (_sort == RASortRegional) {
        NSSortDescriptor *regionSort = [NSSortDescriptor sortDescriptorWithKey:@"region.name" ascending:YES];
        request.sortDescriptors = @[regionSort, countrySort];
        frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:moc sectionNameKeyPath:@"region.name" cacheName:@"Root"];
    } else if (_sort == RASortAlphabetical) {
        request.sortDescriptors = @[countrySort];
        frc = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:moc sectionNameKeyPath:nil cacheName:@"Root"];
    }
    
    _fetchedResultsController = frc;
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

- (void)toggleSort:(RASortToggle)sort {
    _sort = !sort;
    _fetchedResultsController = nil;
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    if (error) NSLog(@"Error performing fetch! - %@", error.localizedDescription);
    [self.delegate updateUI];
}

- (NSInteger)numberOfSections {
    return [self.fetchedResultsController sections].count;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)section {
    return [[self.fetchedResultsController sections][section] numberOfObjects];
}

- (RACountry *)itemForIndexPath:(NSIndexPath *)indexPath {
    RACountry *country = [_fetchedResultsController objectAtIndexPath:indexPath];
    if (country && !country.flagSmall) {
        [self captureTheFlag:country atIndexPath:indexPath];
    }
    return country;
}

- (NSString *)nameForSection:(NSInteger)section {
    RARegion *region = (RARegion *)[self.fetchedResultsController sections][section];
    return region.name;
}

- (void)captureTheFlag:(RACountry *)country atIndexPath:(NSIndexPath *)indexPath {
    RANetworking *networking = [[RANetworking alloc] init];
    [networking flagForCountry:country completion:^(BOOL success) {
        if (success) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate updateIndexPath:indexPath];
            });
        }
    }];
}

- (void)updateCache {
    RANetworking *networking = [[RANetworking alloc] init];
    [networking countries:^(BOOL cancelled, NSError *error) {
        if (!cancelled && !error) {
            NSError *fetchError = nil;
            [self.fetchedResultsController performFetch:&fetchError];
            if (error) {
                NSLog(@"Error performing fetch - %@", fetchError.localizedDescription);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate updateUI];
            });
        }
    }];
    
}

@end
